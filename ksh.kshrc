case "$-" in
*i*)	# We are interactive
	typeset root=${KSH_ETC_ROOT:-/etc/ksh}
	typeset profile

	if test -d ${root}/kshrc.d; then
		for profile in ${root}/kshrc.d/*.ksh; do
			test -r "$profile" && . "$profile"
		done
		unset profile
	fi
	;;

*)	# Non-interactive
	;;
esac

