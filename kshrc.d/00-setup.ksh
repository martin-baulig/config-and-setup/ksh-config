#!/usr/bin/ksh

set -o vi
set -o globstar
set -o noclobber
set -o nounset

ulimit -c 0

if [[ -n $(logname 2>/dev/null) ]]; then
	export LOGNAME=$(logname)
fi

export HOST=$(hostname)

umask 022
