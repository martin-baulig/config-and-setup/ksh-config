if [[ ${force_color_prompt:=no} == yes ]]; then
	if [[ -x /usr/bin/tput ]] && tput setaf >/dev/null; then
		# We have color support; assume it's compliant with Ecma-48
		# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
		# a case would tend to support setf rather than setaf.)
		color_prompt=yes
	else
		color_prompt=
	fi
else
	color_prompt=
fi

typeset vivid=$(which vivid)

# enable color support of ls and also add handy aliases
if [[ -x ${vivid} ]]; then
	export LS_COLORS=$(${vivid} generate molokai)
	alias ls='/usr/local/bin/gls -b --color=auto'
elif [[ -x $(which gdircolors) ]]; then
	test -r ~/.dircolors && eval "$(gdircolors -b ~/.dircolors)" || eval "$(gdircolors -b)"
	if [[ -x /usr/local/bin/gls ]]; then
		alias ls='/usr/local/bin/gls -b --color=auto'
	fi
fi

case "$TERM" in
	xterm | xterm-color | *-256color | linux) color_prompt=yes ;;
esac

if [ "x$color_prompt" = xyes ]; then
	if [ $(id -u) -eq 0 ]; then
		ps1_dollar_color='[01;31m'
		ps1_user_color='[01;31m'
	else
		ps1_dollar_color='[01;33m'
		ps1_user_color='[01;32m'
	fi

	ps1_bracket_color='[01;34m'
	ps1_at_color='[01;36m'
	ps1_host_color='[01;32m'
	ps1_pwd_color='[01;35m'
	ps1_text_color='[0m'
else
	ps1_dollar_color=
	ps1_user_color=
fi

if [ $(id -u) -eq 0 ]; then
	ps1_dollar='#'
else
	ps1_dollar='$'
fi

if [ "x$color_prompt" = xyes ]; then
	PS1='${ps1_bracket_color}[${ps1_user_color}${USER}${ps1_at_color}@${ps1_host_color}${HOST%%.*}${ps1_at_color}:${ps1_pwd_color}${PWD##*/}${ps1_bracket_color}${ps1_at_color} % ${ps1_dollar_color}!] ${ps1_dollar_color}${ps1_dollar}${ps1_text_color} '
else
	PS1='[${USER}@${HOST%%.*}:${PWD##*/} % !] ${ps1_dollar} '
fi
export PS1
