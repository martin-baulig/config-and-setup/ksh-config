for dir in /usr/local/bin /usr/local/ghc/bin $HOME/.local/bin $HOME/.cabal/bin $HOME/.ghcup/bin; do
	#echo "Checking dir $dir ..."
	if test -d $dir; then
		#echo "Adding $dir to PATH."
		PATH=$dir:$PATH
	fi
done

#echo "Final path: $PATH"

