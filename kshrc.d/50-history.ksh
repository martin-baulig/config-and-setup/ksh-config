export HISTSIZE=4096
export HISTEDIT=/bin/ed

function get_histpath
{
	typeset fallback=${XDG_STATE_HOME:-$HOME/.local/state/ksh}
	print -- ${KSH_HISTORY_PATH:-$fallback}
}

function ensure_histpath
{
	typeset histpath=$(get_histpath)
	gmkdir -p -m 0700 -v $histpath
	print -- $histpath
}

function get_global_histpath
{
	print -- $(ensure_histpath)/hist.global
}


function private_hist
{
	typeset histfile=${1:-tmp_$RANDOM}
	export HISTFILE=$(ensure_histpath)/hist.$histfile
	print "Saving history into $HISTFILE."
}

function global_hist
{
	export HISTFILE=$(get_global_histpath)
	print "Saving history globally into $HISTFILE."
}

function save_hist
{
	typeset global_path=$(get_global_histpath)
	typeset current_path=${HISTFILE:-${global_path}}

	if [[ ! -v HISTFILE ]]; then
		print "HISTFILE variable not set."
		return 0
	fi

	if [[ $HISTFILE == ${global_path} ]]; then
		print "Already using the global history."
		return 0
	fi

	if [[ ! -e $HISTFILE ]]; then
		print "Current history file does not exist: $HISTFILE"
		global_hist
		return
	fi

	print "Currently using $HISTFILE."
	(cat $HISTFILE >> ${global_path} && rm -f ${current_path}) || {
		typeset res=$?
		print "Failed to save history: $res"
		return $res
	}

	global_hist
}

function discard_hist
{
	typeset global_path=$(get_global_histpath)
	typeset current_path=${HISTFILE:-${global_path}}

	if [[ ${current_path} == ${global_path} ]]; then
		print "Using global history; not discarding."
		return 0
	fi

	if [[ ! $HISTFILE =~ $(get_histpath)/hist.tmp_([0-9]+) ]]; then
		print "Using custom history path $HISTFILE; not discarding."
		return 0
	fi

	print "Discarding private history file $HISTFILE."

	global_hist
	rm -f $current_path
}

global_hist > /dev/null


