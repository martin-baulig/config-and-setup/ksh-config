alias ll='ls -l'
alias la='ls -la'


function program_alias
{
    typeset name=${1:?"Missing name argument."}
    typeset program=${2:?"Misssing program argument."}
    typeset found=$(which $program)

    if [ -x $found ]; then
        shift 2
        eval alias $name=\'$found $@\'
    else
        print "Missing program: $program"
    fi
}

#program_alias vi nvim
#program_alias cat nvimpager -c
#program_alias less nvimpager

#if [ -x $(which nvimpager) ]; then
#	export MANPAGER=$(which nvimpager)
#fi

